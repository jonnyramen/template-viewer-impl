import React, { useState, useEffect } from "react";
// Relative imports
import "./App.css";

function App() {
  // All of the template data
  const [templates, setTemplates] = useState([]);
  // Used for display of current information
  const [currentTemplateIndex, setCurrentTemplateIndex] = useState(0);
  // Current template information
  const [currentTemplateInfo, setCurrentTemplateInfo] = useState();

  // Filmstrip state
  // Array of current filmstrip templates
  const [currentTemplates, setCurrentTemplates] = useState([]);
  // Index of beginning of current filmstrip
  const [filmstripIndex, setFilmstripIndex] = useState(0);

  useEffect(() => {
    fetch("http://localhost:9000/templates/allTemplates")
      .then((response) => response.json())
      .then((responseJson) => {
        setTemplates(responseJson.data);
        // Set initial values of thumbnail images
        setCurrentTemplates(responseJson.data.slice(0, 4));
        setCurrentTemplateInfo(responseJson.data[0]);
      });
  }, []);

  const LAST_INDEX = templates.length - 1;

  const clickPrevious = () => {
    // Don't try to slice past beginning of templates
    if (filmstripIndex - 4 < 0) return;

    // Set new starting index of filmstrip
    const newTemplateIndex = filmstripIndex - 4;
    // Slice the array to contain up to next 4 images
    setCurrentTemplates(
      templates.slice(newTemplateIndex, newTemplateIndex + 4)
    );
    // Set the new current index
    setFilmstripIndex(newTemplateIndex);
  };

  const clickNext = () => {
    // Don't wrap around; if no more images exist, return
    if (filmstripIndex + 4 >= LAST_INDEX) return;

    // Set new starting index of filmstrip
    const newTemplateIndex = filmstripIndex + 4;
    // Slice the array to contain up to next 4 images
    setCurrentTemplates(
      templates.slice(newTemplateIndex, newTemplateIndex + 4)
    );
    // Set the new current index
    setFilmstripIndex(newTemplateIndex);
  };

  const clickThumbnail = (index) => {
    // Calculate index of clicked template
    const templateIndex = filmstripIndex + index;
    // Set the template info
    setCurrentTemplateInfo(templates[templateIndex]);
    // Set the template index
    setCurrentTemplateIndex(templateIndex);
  };

  return (
    <div className="App">
      {/* Container of selected image and image details */}
      <div className="presentation-container">
        {/* Display of selected large image */}
        <div className="presentation-div">
          <img
            src={`/large/${templates[currentTemplateIndex]?.image}`}
            alt={`${templates[currentTemplateIndex]?.image}`}
          />
        </div>
        {/* Display details of selected template */}
        <div className="presentation-div">
          <h3>Template Details:</h3>
          {/* Details div */}
          <div className="template-details">
            {/* ID, Cost, Description, Thumbnail File Name, Image File Name */}
            <p>ID: {currentTemplateInfo?.id}</p>
            <p>Cost: {currentTemplateInfo?.cost}</p>
            <p>Description: {currentTemplateInfo?.description}</p>
            <p>Thumbnail File Name: {currentTemplateInfo?.thumbnail}</p>
            <p>Image File Name: {currentTemplateInfo?.image}</p>
          </div>
        </div>
      </div>

      <div>
        <img
          className={`previous ${filmstripIndex === 0 ? "disabled" : ""}`}
          onClick={clickPrevious}
          src="/previous.png"
          alt="next"
        />
        <img
          className={`next ${
            filmstripIndex + 4 > LAST_INDEX ? "disabled" : ""
          }`}
          onClick={clickNext}
          src="/next.png"
          alt="next"
        />
      </div>

      {/* Thumbnail images area */}
      <div className="thumbnails">
        {currentTemplates.map((template, index) => (
          <div
            className="thumbnail-container"
            onClick={() => {
              clickThumbnail(index);
            }}
            key={index}
          >
            <img
              alt="thumbnails"
              className={`${
                currentTemplateIndex === filmstripIndex + index ? "active" : ""
              }`}
              src={`/thumbnails/${template.thumbnail}`}
            />
            <a>
              <span
                className={`${
                  currentTemplateIndex === filmstripIndex + index
                    ? "active"
                    : ""
                }`}
              >
                {template.thumbnail}
              </span>
            </a>
          </div>
        ))}
      </div>
    </div>
  );
}

export default App;
