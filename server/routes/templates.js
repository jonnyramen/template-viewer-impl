const express = require("express");
const router = express.Router();

let templates = require("../data/templatesDb");

router.get("/allTemplates", async (req, res) => {
  try {
    res.status(200).json({
      data: templates,
    });
  } catch (err) {
    res.status(400).json({
      message: "Some error occured",
      err,
    });
  }
});
module.exports = router;
