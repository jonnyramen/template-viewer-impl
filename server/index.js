// index.js

const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const logger = require("morgan");
const port = process.env.PORT || 9000;
const templatesRouter = require("./routes/templates");

app.use(logger("dev"));
app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Use templates router
app.use("/templates", templatesRouter);
app.listen(port, function() {
  console.log("Runnning on " + port);
});

module.exports = app;
