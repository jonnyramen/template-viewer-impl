## Website Template Viewer

### Overview

Welcome to the website template viewer. This project is developed with a Node.js back end serving up the metadata regarding the templates
to be rendered out on the front end,
and with a React front end developed from [Create React App](https://create-react-app.dev/).

### Requirements
This README assumes you have [Node.js](https://nodejs.org/en/) installed on your computer.

### Running the Back End
To run the back end, navigate to the `server` directory
```
cd server
```

and execute the `index.js` file using `node`:
```
node index.js
```

### Running the Front End
To run the front end interface, which will execute successfully given that you have first served the back end, first navigate to the client directory:
```
cd client
```

Next, install the packages required:
```
npm install
```

And, once this finishes, execute the `start` command to run the client:
```
npm run start
```

Navigate to [localhost:3000](http://localhost:3000), and enjoy some template viewing!